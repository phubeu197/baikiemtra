﻿namespace laptrinhwepkiemtra.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ContactAndAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }

}
