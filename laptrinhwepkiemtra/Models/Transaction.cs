﻿namespace laptrinhwepkiemtra.Models
{
    public class Transaction
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public Employee? Employee { get; set; }
        public Customer? Customer { get; set; }

    }

}
