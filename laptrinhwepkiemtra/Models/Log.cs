﻿namespace laptrinhwepkiemtra.Models
{
    public class Log
    {
        public int Id { get; set; }
        public int TransactionalId { get; set; }
        public DateTime LoginDate { get; set; }
        public TimeSpan LoginTime { get; set; }
        public Transaction? Transaction { get; set; }
    }

}
