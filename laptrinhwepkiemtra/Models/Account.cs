﻿namespace laptrinhwepkiemtra.Models
{
    public class Account
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string AccountName { get; set; }
        public Customer? Customer { get; set; }
    }

}
