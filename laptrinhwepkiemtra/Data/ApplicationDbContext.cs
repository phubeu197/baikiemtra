﻿using laptrinhwepkiemtra.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace laptrinhwepkiemtra.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Account> Accounts { get; set; }

        public DbSet<Log> Logs { get; set; }

        public DbSet<Report> Reports { get; set; }

        public DbSet<Transaction> Transactions { get; set; }


    }
}
